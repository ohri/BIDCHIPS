<html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: BIDCHIPS: Bias-Decomposition of ChIP-Seq, a reference...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for BIDCHIPS-package"><tr><td>BIDCHIPS-package</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
BIDCHIPS: Bias-Decomposition of ChIP-Seq, a reference implementation of the method of Perkins, Palidwor and Ramachandran for quantifying and removing biases from ChIP-seq data.
</h2>

<h3>Description</h3>

<p>This package accompanies the paper &quot;BIDCHIPS: Bias decomposition and removal from ChIP-seq data clarifies true binding signal and its functional correlates&quot; by Ramachandran, Palidwor and Perkins, Epigenetics &amp; Chromatin, Epigenetics &amp; Chromatin 2015, 8:33. The package generates a linear model based on the value of normalized data across contiguous genome-wide windows; The input data tracks are Mappability, GC-content, and BAM file counts for Treatment, input DNA and IgG controls, and DNaseI hypersensitivity. The output is the purified BAM file counts (estimated using the background model) for the ChIPSeq file of interest.
</p>


<h3>Details</h3>


<table summary="Rd table">
<tr>
 <td style="text-align: left;">
Package: </td><td style="text-align: left;"> BIDCHIPS</td>
</tr>
<tr>
 <td style="text-align: left;">
Type: </td><td style="text-align: left;"> Package</td>
</tr>
<tr>
 <td style="text-align: left;">
Version: </td><td style="text-align: left;"> 1.1</td>
</tr>
<tr>
 <td style="text-align: left;">
Date: </td><td style="text-align: left;"> 2016-01-07</td>
</tr>
<tr>
 <td style="text-align: left;">
License: </td><td style="text-align: left;"> GPL-2</td>
</tr>
<tr>
 <td style="text-align: left;">
</td>
</tr>

</table>



<h3>Author(s)</h3>

<p>Author: Gareth Palidwor
<br />
Maintainer: Gareth Palidwor &lt;gpalidwor@ohri.ca&gt;
</p>


<h3>References</h3>

<p>&quot;BIDCHIPS: Bias decomposition and removal from ChIP-seq data clarifies true binding signal and its functional correlates&quot; by Ramachandran, Palidwor and Perkins, Epigenetics &amp; Chromatin, Epigenetics &amp; Chromatin 2015, 8:33
</p>


<h3>Examples</h3>

<pre>
#Code for loading human hg19 chromosome 22 windows of data and generating linear model
#data downloadable from Perkins' Lab web site (http://dropbox.ogic.ca/Perkins_BIDCHIPS/hg19chr22BIDCHIPS_rawdata.zip).
#Once the linear model and data are generated, we can apply the linear model to the data
#using the predict function.
library("BIDCHIPS")
library("BSgenome.Hsapiens.UCSC.hg19")
chr22WindowModel&lt;-
    buildModel(
      MappabilityBwFile="wgEncodeCrgMapabilityAlign36mer_chr22.bw",
      inBamFilesList=list(
        FNmDNaseI="wgEncodeUwDnaseGm12878AlnRep1_chr22.bam",
        FNmiDNA="wgEncodeSydhTfbsGm12878InputStdAlnRep1_chr22.bam",
        FNmIgGCtrl="wgEncodeSydhTfbsGm12878InputIggrabAlnRep1_chr22.bam"),
      outBamFileList=list(
        FNmTrt="wgEncodeHaibTfbsGm12878Atf3Pcr1xAlnRep1_chr22.bam"
      ),
      modeloutfile="hg19chr22model.object",
      windowsoutfile="hg19chr22windows.object",
      regions="chr22",ReadLen=36,cores=3,WinSz=257,bsgenome=Hsapiens
)



#Code for loading human hg19 windows of data based on peak regions and generating linear model and data windows
#data downloadable from Perkins' Lab web site (http://dropbox.ogic.ca/Perkins_BIDCHIPS/hg19allBIDCHIPS_rawdata.zip)
summits&lt;-read.table("wgEncodeHaibTfbsGm12878Atf3Pcr1xAlnRep1_summits.bed",header=F)[,1:3]
colnames(summits)&lt;-c("PeakChr","SummitStart0based","SummitEnd1based")
summits&lt;-subset(summits,!(PeakChr 
summit_order&lt;-order(match(summits$PeakChr,allchroms),summits$SummitStart0based)
summits&lt;-summits[summit_order,]
WinSz=257
HalfWin=(WinSz/2)-1
peaksummit_gr&lt;-GRanges(seqnames=summits$PeakChr,
                       IRanges(start=summits$SummitStart0based-HalfWin,
                              end=summits$SummitStart0based+HalfWin),
                        seqlengths=chromlens[allchroms])
peakModel&lt;-
  buildModel(
    MappabilityBwFile="wgEncodeCrgMapabilityAlign36mer.bw",
    inBamFilesList=list(
      FNmDNaseI="wgEncodeUwDnaseGm12878AlnRep1.bam",
      FNmiDNA="wgEncodeSydhTfbsGm12878InputStdAlnRep1.bam",
      FNmIgGCtrl="wgEncodeSydhTfbsGm12878InputIggrabAlnRep1.bam"),
    outBamFileList=list(
      FNmTrt="wgEncodeHaibTfbsGm12878Atf3Pcr1xAlnRep1.bam"
    ),
    modeloutfile="hg19peakmodel.object",
    windowsoutfile="hg19peakwindows.object",
    regions=peaksummit_gr,ReadLen=36,cores=2,WinSz=257,Hsapiens
  )


#code for loading all human hg19 windows of data data and generating linear model and data windows
#data downloadable from Perkins' Lab web site (http://dropbox.ogic.ca/Perkins_BIDCHIPS/hg19chr22BIDCHIPS_rawdata.zip)
library(BSgenome.Hsapiens.UCSC.hg19)
allchroms&lt;-grep("chr[123456789X0]+$",seqnames(BSgenome.Hsapiens.UCSC.hg19),value=T)
allWindowModel&lt;-
  buildModel(
    MappabilityBwFile="wgEncodeCrgMapabilityAlign36mer.bw",
    inBamFilesList=list(
      FNmDNaseI="wgEncodeUwDnaseGm12878AlnRep1.bam",
      FNmiDNA="wgEncodeSydhTfbsGm12878InputStdAlnRep1.bam",
      FNmIgGCtrl="wgEncodeSydhTfbsGm12878InputIggrabAlnRep1.bam"),
    outBamFileList=list(
      FNmTrt="wgEncodeHaibTfbsGm12878Atf3Pcr1xAlnRep1.bam"
    ),
    modeloutfile="hg19allmodel.object",
    windowsoutfile="hg19allwindows.object",
    regions=allchroms,ReadLen=36,cores=2,WinSz=257,Hsapiens
  )
predictedVals&lt;-predict(chr22WindowModel$model,chr22WindowModel$windows)

#code for applying a model to windows and generating a bigwig file from it
#for visualization
#NOTE: the bigwig files are extremely large and will take a while to generate
library(stats)
predictedVals&lt;-predict(chr22WindowModel$model,chr22WindowModel$windows)
residualVals&lt;-chr22WindowModel$windows$FNmTrt-predictedVals
predicted_gr&lt;-GRanges(seqnames=chr22WindowModel$windows$chrom,
                      IRanges(
                      start=chr22WindowModel$windows$start,
                      end=chr22WindowModel$windows$end),
                      score=predictedVals,
                      seqlengths=seqlengths(BSgenome.Hsapiens.UCSC.hg19))
residual_gr&lt;-GRanges(seqnames=chr22WindowModel$windows$chrom,
                      IRanges(
                      start=chr22WindowModel$windows$start,
                      end=chr22WindowModel$windows$end),
                      score=residualVals,
                      seqlengths=seqlengths(BSgenome.Hsapiens.UCSC.hg19))
observed_gr&lt;-GRanges(seqnames=chr22WindowModel$windows$chrom,
                      IRanges(
                      start=chr22WindowModel$windows$start,
                      end=chr22WindowModel$windows$end),
                      score=chr22WindowModel$windows$FNmTrt,
                      seqlengths=seqlengths(BSgenome.Hsapiens.UCSC.hg19))
#note that generate bigwig files can be quite big
export.bw(predicted_gr,"predicted.bw")
export.bw(observed_gr,"observed.bw")
export.bw(residual_gr,"residual.bw")
</pre>


</body></html>
